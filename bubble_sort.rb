def bubble_sort(arr)
  i = 0
  while i < arr.length - 1
    j = 0
    while j < arr.length - 1
      arr[j], arr[j + 1] = arr[j + 1], arr[j] if arr[j] > arr[j + 1]
      j += 1
    end
    i += 1
  end

  arr
end

p bubble_sort([4, 3, 78, 2, 0, 2])
p bubble_sort([10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0])
