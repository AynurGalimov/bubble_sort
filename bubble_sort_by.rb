def bubble_sort_by(arr)
  i = 0
  while i < arr.length - 1
    j = 0
    while j < arr.length - 1
      left = arr[j]
      right = arr[j + 1]
      arr[j], arr[j + 1] = arr[j + 1], arr[j] if yield(arr[j], arr[j+1]) < 0
      j += 1
    end
    i += 1
  end

  arr
end

result = bubble_sort_by(["hi","hello","hey"]) do |left,right|
  right.length - left.length
end

p result